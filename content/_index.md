---
title: "About Me"
# date: 2021-01-05T00:00:01+01:00
# draft: false
hideLastModified: true
# showInMenu: true
# no need for the "summary" parameter as it is not displayed in any previews

---

### Scolarship

I am a Postdoc at the joint lab between [CNRS](https://www.cnrs.fr/) and [Solvay](https://www.solvay.fr/) called the [Laboratory Of the Future](https://www.solvay.fr/implantations/bordeaux-recherche). This Postdoc is conducted in collaboration with the Auctus team. The goal of this project is to introduce a robotic assistant inside a glovebox. To know more about this project you can go .

From 2019 to 2021 I worked on the [Harry² project]({{< ref "/research/Postdoc Work/Harry2" >}}) in the [Auctus](https://auctus-team.gitlabpages.inria.fr/) team. From 2015 to 2018 I did my PhD in medical Robotic under a Cifre contract between [General Electric Healthcare](https://www.gehealthcare.fr/) and the [Institute of Intelligent Systems and Robotics](https://www.isir.upmc.fr/) of the [Sorbonne Univeristé](https://www.sorbonne-universite.fr/). In 2015 I received a master degree in Mechatronics from the Ecole Normale Superieur de Cachan.

### My research

My research is focused on the expression of control laws for robotic manipulators to realise complexed tasks in a dynamic environment. To do so I use quadratic programming problem formulations. This allows to respect constraints that can be intrinsic to the robot (*e.g* the joint position limits, joint velocity limits, ...) or extrinsic (*e.g* virtual walls, ...) while performing a set of tasks as best as possible. 

I enjoy doing experimental work. All the projects I worked on involve interactions with collaborative robot.


### My fonctions

Since I arrived in the Auctus team, I helped extend the experimental platform in parallel of my research. To do so, I developped the control architecture of the two Franka Emika panda robots, including the many sensors used around it (RGBD cameras, laser sensor, F/T sensor, ...). This control architecture mainly uses ROS with the many tools along side. 

I developped two libraries to formulate QP problem for robot manipulators at the joint velocity and torque level: [torque_qp](https://gitlab.inria.fr/auctus-team/components/control/torque_qp) and [velocity_qp](https://gitlab.inria.fr/auctus-team/components/control/velocity_qp). All the softwares developped for the team are open source and free of use and can be found [here](https://gitlab.inria.fr/auctus-team/components).

I'm also handling the technical aspects of the Auctus team (wifi, computer inventory, ...).

Finnaly, I'm administrating the Auctus gitlab account. 

