---
title: "Credits"
date: 2019-06-17T23:53:00+01:00
draft: false
hideLastModified: true
# no need for the "summary" parameter as it is not displayed in any previews
---

I used the [hugo-refresh](https://github.com/PippoRJ/hugo-refresh) theme for this website. I modified it to better suit my liking.