---
title: "Hanoi"
date: 2019-01-06T23:53:00+01:00
draft: false
hideLastModified: true
summary: "A showcase of the Auctus team research topic throug the Tower of Hanoi problem"
summaryImage: "Resources/concept.jpg"
tags: ["custom_image", "custom_summary"]
---

In 2021 Auctus has developed a platform to be used as a showcase of the team activities. This platform evolves around the problem of the tower of Hanoi which require both physical interaction with the world and thought process. One or several robots are used to assist a human in solving the problem. This plateform will allow the team to presents there contribution in both the cognitive (HRI) and physical (pHRI) aspects of human/robot interaction.

The Tower of Hanoi is a mathematical game or puzzle consisting of three rods and a number of disks of various diameters, which can slide onto any rod. The puzzle begins with the disks stacked on one rod in order of decreasing size, the smallest at the top, thus approximating a conical shape. The objective of the puzzle is to move the entire stack to the last rod, obeying the following rules:

- Only one disk may be moved at a time.
- Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack or on an empty rod.
- No disk may be placed on top of a disk that is smaller than it.

{{< figure src="Resources/concept.jpg" width="700" caption="Tower of Hanoi" >}}


The platform is designed to be fully autonomous. This means that external sensors must be used to sense the environment and get the state of the problem in real time as well as the presence of humans. A visual solution based on the use of a monocular camera and Apriltag markers was chosen. After a study on the type of Apriltag markers best suited to the project, a framework based on ROS packages was developed.

This framework includes the generation and recognition of the Apriltag markers, the calibration of the intrinsic and extrinsic parameters of the camera as well as the position of the camera with respect to the robot and the generation and processing of the camera video stream.Trajectory generation algorithm must be implemented to adapt online the next trajectory of the robot depending on the state of the game. Control algorithms being at the core of Auctus, our state of the art tools are being used inside the project. A state machine using PDDL is used to solve the tower of hanoi problem. A second state machine is implemeted to supervise the correct execution of the robot tasks.

In the first version of this platform the robot must solve the problem without any human intervention and without any knowledge of the initial state of the problem. Here is a video of this initial version.

{{< vimeo 607306647 >}}

Future development on this showcase will involve:
- use of an RGBD camera to refine the position of the Apriltag markers in space;
- accounting for a human in the scene:
- the robot could still solve the problem alone, as fast as possible, but reduce its velocity
depending on the distance to human to ensure safety;
    - it could also be used to assist the human into solving the problem;
    - it could also change its behavior to actively solve the problem alongside the human;
- two robots working on the problem and sharing their tasks;
- add cognitive markers such as a breathing behavior on the redundant axes. Or external sensor (leds,
pushbutton, ...) to enhance the interaction with the robot.