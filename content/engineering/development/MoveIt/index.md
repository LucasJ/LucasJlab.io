---
title: "MoveIt"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Integration of MoveIt in our control environment"
summaryImage: "Resources/moveit.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
math: true
---

MoveIt is a software that incorporates the latest advances in motion planning, manipulation, 3D perception, kinematics, and navigation. The control part in MoveIt is a bit lacking compared to state of the art QP solution. Still, MoveIt can compute trajectories avoiding obstacles and ensuring the respect of the robot kinematic constraint. This is particulary interesting for projects such as the [instrumented glovebox]({{< ref "research/Postdoc Work/Instrumented glovebox" >}}).

This package uses moveit functions to expose a moveit trajectory. By default moveit doesn't provide a trajetory as an object that can be called for any time step along a path. Instead it sends a trajectory_msgs with waypoints specifying a target position, velocity and acceleration. For the control tools developped by Auctus this is an issue as a trajectory must be callable for any time step.

This package uses an action server to get a trajectory_msgs planned by MoveIt and uses [toppra](https://github.com/hungpham2511/toppra) tools to define a trajectory that can be called for any time. This action server is called by a moveit controller manager node.

The trajectory object is created inside the robot control loop (see https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control for example) and an updateTrajectory() function can be run periodically. You can specify the time increment of the trajectory with the setTrajectoryTimeIncrement() function. A separate thread is launched to run the action server.

The controller is used seamlessly inside moveit. Examples of how to define trajectories are presented in the test folder.

ADD VIDEO

Test must be made to verify the ability to detect obstacle and avoid them using moveit with the QP controller. To do so a proper calibration of the camera position relatively to the robot needs to be done.

This package is open source and can be found [here](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface)