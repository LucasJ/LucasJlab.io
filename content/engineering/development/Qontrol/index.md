---
title: "Controller using Quadratic programming formulations"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Librairies to control a robot manipulator using quadratic programming."
summaryImage: "Resources/Under-Construction.jpg"
tags: ["custom_image", "custom_summary"]
layout : single
math: true
---

<!-- My research is strongly focused on determining control laws for safe human robot interaction in dynamic environment. These control laws rely on the formulation of the problem as a quadratic programming under constraints. During my postdoc at Inria I develop some tools to simply the formulation of such problems for serial manipulators.

torque_qp to control in torque, velocity_qp to control the joint velocities. (With links)

Aim of these packages

Integration within the rest of the softwares developped. With some videos.

Sharing of the code with other teams/labs. 

Qontrol -->