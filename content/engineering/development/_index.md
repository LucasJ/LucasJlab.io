---
title: "Development"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Keep a minimal distance between a human an a robot through a QP constraint"
summaryImage: "development.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
layout : list
math: true
---