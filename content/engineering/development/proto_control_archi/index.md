---
title: "Control architecture prototype"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "A control architecture protoype to test simple ideas on a 2D robot before going on more complex robots"
summaryImage: "Resources/preview.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
math: true
---

All the softwares I developed are intended for robotic manipulators with several degrees of freedom. These are the robots mostly used in the industry because they are move versatile to realize various tasks. However from a development point of view it can be hard to implement new control laws on such system. The more complexe the system is the hard it is to find errors in the early development stage. When testing new concepts, it is much simpler to test them on smaller systems. 

This projects aims at creating a whole robot control architecture for a simple robot evolving in a 2d world. The robot can move freely in the (x,y) plane and evolves in an environment with moving obstacles. The robot has to go from a pose (0,0) to (1,1). The following control scheme present the developped control architecture.

{{< figure src="Resources/architecture_scheme" width="700" caption="Architecture" >}}

The objective is to have a modular control architecture where it is easy to replace a component with another one. The current implementation is as follow:
- The **path** is computed using OMPL
- The **trajectory** is computed using Toppra
- The **task dynamic** is computed via a simple PID controller
- The **quadratic programming** problem is solved with OSQP
- **Obstacles** can move along a line at a specified velocity

The path and the trajectory can be recomputed online at specified frequencies.

Here is a video of the latest advancement on this project :
