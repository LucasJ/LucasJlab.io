---
title: "Realtime control architectures"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Keep a minimal distance between a human an a robot through a QP constraint"
summaryImage: "preview.png"
tags: ["custom_image", "custom_summary"]
layout : single
math: true
---

# PhD

All the work presented in the PhD section are performed on a realtime enabled machine in a mixed environment between OROCOS and ROS. The code can be found here.

# Postdoc

During my postdoc at INIRA I implemented a realtime control architecture for the two Panda robots present in the lab. I used OROCOS along side ROS. The code can be found here

### On two real robots

Here is a video of two panda robots being control synchronously. The robot on the right is in gravity compensation. The one on the left must follow the joint configuration of the other robot. Both robots run at 1 kHz.

{{< vimeo 471903033 >}}

### In simulation

The control architecture also works in simulation on gazebo as shown in the video bellow.

{{< vimeo 471903346 >}}
