---
title: "Robots and sensors"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Keep a minimal distance between a human an a robot through a QP constraint"
summaryImage: "preview.png"
tags: ["custom_image", "custom_summary"]
layout : single
math: true
---

{{< rawhtml >}}

<h3> This is a list of all the robot I worked with </h3>

<style>
details > summary {
  list-style: none;
  padding-top: 50px
}
details > summary::-webkit-details-marker {
  display: none;
}

.column {
  float: left;
  width: 50%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

#Parent{
    line-height: 200px; // > the highest element to be centered.
}

.parent img {
    vertical-align: middle;
}

ul {
  list-style: none;
  display: flex;
  justify-content: space-around;
  height:300px;
  margin: 0;
  padding: 0;
}

li {
  margin: 0rem;
}

img {
  max-width: 100%;
  max-height: 100%;
}

figure {
    display: inline-block;
    /* border: 1px dotted gray; */
    margin: 20px; /* adjust as needed */
}
figure img {
    vertical-align: bottom;
}
figure figcaption {
    /* border: 1px dotted blue; */
    text-align: center;
    /* height:30px; */
}

</style>

<ul>
    <figure><img style="height:300px;" src="Resources/panda.png" alt="panda"> <figcaption><br><b> Panda </b> </figcaption></figure>
    <figure><img style="height:300px;" src="Resources/comau.png" alt="comau"> <figcaption><br><b> Comau Racer 3 </b> </figcaption></figure>
    <figure><img style="height:300px;" src="Resources/wam.png" alt="wam"> <figcaption><br><b> Barret wam </b> </figcaption></figure>
    <figure><img style="height:300px;" src="Resources/lwr4.png" alt="lwr4"> <figcaption><br><b> KUKA LWR4+ </b> </figcaption></figure>
</ul>
<ul>
    <figure><img style="height:300px" src="Resources/iiwa.jpg" alt="iiwa"> <figcaption><br><b> KUKA IIWA </b> </figcaption></figure>
    <figure><img style="height:300px" src="Resources/ur16e.png" alt="ur16e"> <figcaption><br><b> Univeral Robot </b> </figcaption></figure>
    <figure><img style="height:235px" src="Resources/doosan.png" alt="doosan"> <figcaption><br><b> Doosan </b> </figcaption></figure>
</ul>

<hr>

<h3> This is a list of all the sensors I used. </h3>


<ul>
    <figure> <img style="height:150px;" src="Resources/kinect.jpg" alt="kinect"> <figcaption><br><b> RGBD sensors </b> </figcaption></figure>
    <figure> <img style="height:150px;" src="Resources/keyence.png" alt="keyence"> <figcaption><br><b> Keyence Safety
Laser Scanner *</b> </figcaption></figure>
    <figure> <img style="height:150px;" src="Resources/iidre.png" alt="iidre"> <figcaption><br><b> IIDRE UWB sensors *</b> </figcaption></figure>
    <figure> <img style="height:150px;" src="Resources/optitrack.jpg" alt="optitrack"> <figcaption><br><b> OptiTrack </b> </figcaption></figure>
</ul>

<ul>
    <figure> <img style="height:150px;" src="Resources/ATI.png" alt="ATI F/T sensors"> <figcaption><br><b> ATI Force/Torque sensors </b> </figcaption></figure>
    <figure> <img style="height:150px;" src="https://upload.wikimedia.org/wikipedia/commons/8/87/Arduino_Logo.svg" alt="arduino"> <figcaption><br><b> Multiple sensors compatible with Arduino </b> </figcaption></figure>
</ul>

* I developped ROS drivers to use them in my work.

{{< /rawhtml >}}