---
title: "Softwares"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Keep a minimal distance between a human an a robot through a QP constraint"
summaryImage: "software.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
layout : single
math: true
---

This is a list of all the software and libraries that I uses on a daily basis for my robotic development. You can click on the logo to get more information on the softwares.

{{< rawhtml >}}
<style>
details > summary {
  list-style: none;
  padding-top: 50px;
}

.column {
  float: left;
  width: 50%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
 
<details open> <summary> <h2> Robotics </h2> </summary>
    <div class="row">
        <div class="column">
            <details>
                <summary><img style="max-height:100px"
                src="Resources/ros_logo.png"
                alt="ROS"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    The Robot Operating System (ROS) is a set of open source software libraries and tools that help roboticist build robot applications. From drivers to state-of-the-art algorithms, and with powerful developer tools, ROS greatly helps in the developpement of robotics project.
                </div>
            </details>
            <details>
                <summary><img style="height:100px"
                src="Resources/gazebo_logo.png"
                alt="Gazebo"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    Gazebo is a dynamic simulator using state of the art physic engin to efficiently simulate populations of robots in complex indoor and outdoor environments. Its compatibility with ROS makes it a standard simulation to test the behaviour of the robotic application that I build.
                </div>          
            </details>
            <details>
                <summary><img style="height:100px"
                src="https://github.com/facontidavide/PlotJuggler/blob/main/docs/plotjuggler3_banner.svg?raw=true"
                alt="PlotJuggler"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    PlotJuggler is a tool to visualize time series that is fast, powerful and intuitive. It is compatible with ROS and a useful tool to visualize ROS topics.
                </div>          
            </details>
        </div>
        <div class="column">
            <details>
                <summary><img style="height:100px"
                src="https://moveit.ros.org/assets/logo/moveit_logo-black.png"
                alt="Moveit"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    MoveIt is a software that incorporates the latest advances in motion planning, manipulation, 3D perception, kinematics, and navigation. The control part in MoveIt is a bit lacking compared to state of the art QP solution. In my work with INRIA I developped an interface to use MoveIt along side our QP controllers.
                </div>
            </details>
            <details>
                <summary>
                <div>
                    <img style="vertical-align:middle;height:100px" src="https://www.orocos.org/files/logo-t.png" alt="OROCOS"> 
                    <span style="font-size:500%;vertical-align: middle;"> &nbsp; OROCOS </span >
                </div>
                </summary>
                <div style="padding-right:50px;padding-top:20px;">
                    The Open Robot Control Software (Orocos) is a set of tools for development of robotics software. I use OROCOS to write real-time component in C++.
                </div>
            </details>
        </div>
    </div>
</details>

<hr>

<details open> <summary> <h2> Math tools </h2> </summary>
    <div class="row">
        <div class="column">
            <details>
                <summary><img style="height:100px;vertical-align: middle;"
                src="https://github.com/stack-of-tasks/pinocchio/blob/master/doc/images/pinocchio-logo-large.png?raw=true"
                alt="pinocchio"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    Pinocchio instantiates the state-of-the-art Rigid Body Algorithms for poly-articulated systems based on revisited Roy Featherstone's algorithms. It is the library use to compute all our robot models.
                </div>
            </details>
            <details>
                <summary><span style="font-size:450%;vertical-align: middle;"> qpOASES </span ></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    qpOASES is an open-source C++ implementation of the recently proposed online active set strategy, which was inspired by important bservations from the field of parametric quadratic programming (QP).
                </div>
            </details>
        </div>
        <div class="column">
            <details>
                <summary>
                        <img style="vertical-align:middle;height:100px" src="Resources/eigen_logo.png" alt="Eigen"> 
                        <span style="font-size:400%;vertical-align: middle;"> &nbsp; Eigen </span >
                </summary>
                <div style="padding-right:50px;padding-top:20px;">
                    Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms. It is heavily used inside most of the code I write.
                </div>
            </details>
            <details>
                <summary><img style="height:100px"
                src="https://osqp.org/assets/images/logo.png"
                alt="OSQP"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    The OSQP (Operator Splitting Quadratic Program) solver is a numerical optimization package for solving convex quadratic programs.
                </div>
            </details>
        </div>
    </div>
</details>

<hr>

<details open> <summary> <h2> Code tools </h2> </summary>
    <div class="row">
        <div class="column">
            <details>
                <summary>
                    <div>
                        <img style="vertical-align:middle;height:100px" src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg" alt="VS code"> 
                        <span style="font-size:300%;vertical-align: middle;"> &nbsp; Visual Studio Code </span >
                    </div>
                </summary>
                <div style="padding-right:50px;padding-top:30px;">
                    Visual Studio Code is a code editor redefined and optimized for building and debugging modern web and cloud applications.
                </div>
            </details>
        </div>
        <div class="column">
            <details>
                <summary><img style="height:100px"
                src="https://upload.wikimedia.org/wikipedia/commons/e/e1/GitLab_logo.svg"
                alt="Gitlab"></summary>
                <div style="padding-right:50px;padding-top:20px;">
                    GitLab is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more.
                </div>
            </details>
        </div>
    </div>
</details>

{{< /rawhtml >}}