---
title: "Harry²"
date: 2021-01-01T23:53:00+01:00
draft: false
hideLastModified: true
summary: "The harry² project"
summaryImage: "Resources/harry2.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
---

The HARRY2 project https://twitter.com/HARRY2_COVR takes place within the framework of the H2020 COVR european project https://safearoundrobots.com/. This project is done in collaboration with the [Robioss team](https://pprime.fr/la-recherche/genie-mecanique-systemes-complexes/robotique-biomecanique-sport-sante-robioss/) of the [PPrime institue](https://pprime.fr/), and [Fuzzy Logic Robotics](https://www.flr.io/). Its objective is to attain more advanced workspace sharing capabilities through fully exploiting the collaborative possibilities defined by ISO TS 15066. This is achieved in the first milestone of HARRY2, using a shared workspace industrial scenario, by:
- Developing PLC software and motion controllers using robot-agnostic industrially-rated components to ease and standardize the development of safe robotic applications with workspace sharing. 
- Integrating state-of-the-art energy-based control algorithms using these industrial hardware components, so that safety is no longer treated as an exception but considered as a constraint when computing the control solution in real-time. 
- Enabling the use of high-level and intuitive teaching interfaces reducing robot programming time and difficulty.

The following video shows the results obtained on a Comau Racer 3 robot

{{< vimeo 471897520>}}

The seconds milestone of the project covers two apects:

- The description of the work performed in the FuzzyStudio software in order to allow for the inclusion of virtual
wall constraints as well as a description of the inclusion of the defined constraints at the control level;
- Our feedback on the norm and protocol associated to transient contact safety evaluation.

The followig video shows the results obtained:

{{< vimeo 471897502>}}
