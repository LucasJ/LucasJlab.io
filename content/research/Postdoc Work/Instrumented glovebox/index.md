---
title: "Instrumented glovebox"
date: 2021-01-01T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Instrumented glovebox"
summaryImage: "Resources/glovebox_sim.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
---

Solvay is a Belgian multinational chemical company established in 1863. Following its integration with Rhodia, the Committee of Executive Members at Solvay reorganised its various business units into five segments – Consumer Chemicals, Advanced Materials, Performance Chemicals, Functional Polymers and Corporate & Business Services.

Solvay has a collaboration with CNRS through a Joint Lab name the [Laboratory Of the Future](https://www.solvay.fr/implantations/bordeaux-recherche)(Lof). Technicians at LOF handle chemicals elements that need to be contained in specific environment. To that extent they use glove boxes . The space inside these boxes is constrained and manipulation of objects is not always practicle. To improve the technician experience, they want to introduce robots inside the glove boxes to assist the lab technician.

{{< figure src="Resources/glovebox_example.jpg" width="700" caption="An example of a glovebox" >}}

A demonstrator was developped with INRIA and Panda robots. The robot must realise three task :

- Bring objects to the technician
- Carry objects to the technician
- Bring objects to specific equipment

In this experiment, a plexiglass panel is used to mimick a glovebox. The robot bring a container with vials, it then hold a vial for a human then bring it to a scale. It then go back to its resting pose.

Since the technician's hand are occupied, the interaction with the robot is done by voice. The [SpeechRecognition](https://pypi.org/project/SpeechRecognition/) python package is used in this project.

{{<vimeo 698513521>}}

The demonstrator was shown at Solvay and generated interest. More simulations need to be done to test this demonstrator. The Panda robot is too big to fit inside a glovebox. Instead a UR3 robot coulbe used. To cover most of the glovebox workspace,the robot could be placed on a rail on the ceiling as depicted in the following figure.

{{< figure src="Resources/glovebox_sim.png" width="700" caption="A simulation in a real glovebox" >}}

Two RGBD cameras can be used to detect the environment inside the gloveblox, QR code are put on the objects of interest to identify and localise them. MoveIt is used to avoid obstacle during the motion. The implementation of MoveiIt inside our QP controllers is detailed [here]({{< ref "/engineering/development/moveit" >}}).