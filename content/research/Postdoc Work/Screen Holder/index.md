---
title: "A screen holder for laparoscopic surgery"
date: 2019-01-06T23:53:00+01:00
draft: false
hideLastModified: true
summary: "A screen holder for laparoscopic surgery"
summaryImage: "Resources/screen_holder.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
---

Laparoscopic surgery or minimally invasive surgery is an operation performed in the abdomen or pelvis using small incisions with the aid of a camera. There are a number of advantages to the patient with laparoscopic surgery including: reduced pain due to smaller incisions, reduced hemorrhaging, and shorter recovery time. However from the surgeon point of view, this operation can be very constraining. Among the drawbacks, the surgeon is now visualizing is now operating through a screen recording the inside of the patient. The surgeon has to move around the patient when operating while the screen stays fix. Several screens can be included in the room but still, the surgeon sometimes needs to adopt unconvenient postures to correctly perform its operation while looking at the screen. 

{{< figure src="Resources/concept.jpg" width="700" >}}

The objective of this postdoc is to put a screen on a robot manipulator and adapt its position according to the surgeon posture. A RBGD sensor is used to detect its hands position shoulders position. Based on this information the robots tries to align the screen in the direction of the surgeon. 

Three experiments were tested. In the first one, the robot tries to align the screen with the vector normal to the plan composed of the vector linking the surgeon **hand** and the vector normal to the ground. In the second one it align the screen with the vector normal to the plan composed of the vector linking the surgeon **shoulder** and the vector normal to the ground. In the last one, the robot moves around a **cylinder** centered to the trocar and aligned with the syrgeon shoulders.

{{< threeVideos id1="552898455" id2="552897959" id3="552898234" caption1="hand alignment" caption2="Shoulder alignement" caption3="Moving around a cylinder">}}

The videos show that it is possible to align the screen according the human posture. In the first two videos, the robot is in a constant collaborative mode. It can be hold and move wherever the surgeon wants it to be. After 5 seconds of being still, the robot revert to a control mode where it tracks the surgeon posture. This feature is not present in the cylinder tracking mode for obvious reason. This last mode is interesting but requires a robot with an important workspace. The postdoc only lated 6 months and no further validation could be performed with surgeons.