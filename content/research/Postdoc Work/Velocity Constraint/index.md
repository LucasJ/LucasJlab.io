---
title: "Velocity constraint"
date: 2020-01-07T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Velocity constraint"
summaryImage: "Resources/ICRA2020.png"
keepImageRatio: true
tags: ["custom_image", "custom_summary"]
---

Despite the many advances in collaborative robotics, collaborative robot control laws remain similar to the ones used in more standard industrial robots, significantly reducing the capabilities of the robot when in proximity to a human. Improving the efficiency of collaborative robots requires revising the control approaches and modulating online and in real-time the low-level control of the robot to strictly ensure the safety of the human while guaranteeing efficient task realization. In this work, an openly simple and fast optimization based joint velocity controller is proposed which modulates the joint velocity constraints based on the robot’s braking capabilities and the separation distance. The proposed controller is validated on the 7 degrees-of-freedom Franka Emika Panda collaborative robot. The following work was presented in [IROS 2020](https://hal.archives-ouvertes.fr/hal-02434905v3/document)

The experimental setup uses a Kinect sensor to detect the human position relatively to the robot as depicted in the following figure.

{{< figure src="Resources/ICRA2020.png" width="700" caption="The experimental setup" >}}

A constraint on the robot joint velocity is implemented in the controller. The velocity limits is determined as a function of an estimation on the robot braking capabilities and the distance to the human. The objective is for the robot to decelerate only when necesary, i.e when there is a risk of the human entering the robot workspace (consider as a sphere center on the robot base link).

The following video present the work accomplished in more details.
{{< vimeo 459324558 >}}