---
title: "Projected mass minimization"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Online minimization of a robot projected mass."
summaryImage: "Resources/Mass_minimization.jpg"
tags: ["custom_image", "custom_summary"]
---

{{< figure src="summary.jpg" width="700" >}}

When moving, a robot projected mass, M in Cartesian space is closely linked to its kinetic energy through the relation Ek=Mv2

. This mass is also function of the robot configuration. For redundant robot it is thus possible to find a configuration which minimizes this projected mass.

This experiment shows that is is possible to find such configurations online and how it can help reducing the robot equivalent mass