---
title: "Distance constraint"
date: 2018-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Keep a minimal distance between a human an a robot through a QP constraint"
summaryImage: "Resources/distance_constraint.png"
tags: ["custom_image", "custom_summary"]
math: true
---

A way to ensure that no unwanted contact with a robot can occur with a human is to enforce a limit distance between the two. In this experiment,  the localization of an obstacle in the robot environment is observed using RGBD cameras. If the distance, $d$, between the obstacle and the robot is less than a defined limit, $d_{lim}$, a new desired position is computed to keep the robot at a safe distance. Let $\boldsymbol{u}_{obs} \in \mathbb{R}^3$ be a unit vector representing the direction of the obstacle towards the robot. $\boldsymbol{X}_s^{goal}$ is a goal, fixed position, to reach. 

For this experiment, the robot is required to stay at a fixed point. A human moves close to the robot and is considered as an obstacle. A safe distance $d_{lim}=0.4$ m is enforced. A constraint on the robot workspace could also be added as described [here]({{< ref "/research/phd_work/virtual_wall" >}}). If the human passes through the constrain, the QP won't be able to find a solution and the robot will have to perform an emergency stop. A safety limit $d_{safe} < d_{lim}$ should be used to allow some slack for the redefinition of the desired pose. 

{{< vimeo 471886590>}}

The results of this experiment are the following:

{{< figure src="Resources/figure.png" width="700" >}}

 When the human is out of the detection range, the distance, $d$, is null and both the pointing and the positioning task errors are stable with a mean positioning error of 0.4 $mm$ and a mean pointing error of $0.7$ mm. When the human reaches the safe distance limit, a new desired position is defined to keep the robot away from the obstacle. It can be seen that this on-line redefinition of the desired position successfully keeps the robot at a safe distance. 

