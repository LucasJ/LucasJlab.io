---
title: "Impact energy measurment"
date: 2017-01-07T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Measuring the energy released by a robot during an impact."
summaryImage: "Resources/platform.png"
tags: ["impact", "experimental"]
math: true
---


A plateform was built to validate the kinetic energy constraint proposed previously. This platform measures the elongation of a spring through a set of encoder to determine the kinetic energy displayed during the impact.

{{< figure src="Resources/geometric_view.png" width="700" >}}

It can be showed that the elongation of the spring, $\Delta x$ , can be expressed as 

$$ \Delta x(\theta) = \sqrt{b^2 + d^2 - 2bdcos(\phi+\theta)} - c . $$

In the developed platform, the spring stiffness is measured beforehand using a micrometric table and a dynamometer with a 0.05N resolution. It resulted a measured stiffness of 738 N/m. The distance b is equal to 0.236 m. The spring is preloaded to ensure that it elongates as soon as the contact occurs. To do so a rubber band with a negligible stiffness is used. The spring nominal length is measured with a calliper and is equal to 0.1005 m. The encoder used is an AMT 102 13-bit encoder. The platform is made modular so that the energy can be measured with different robot configurations. Using beams with different size, the robot can also collide with the platform at different heights. During the experiments, the 6$^{th}$ axis of the robot will enter in collision with the platform and an ATI F/T sensor, placed at the robot end-effector (see Figure below), will record the inertial forces acting on the robot.

{{< figure src="Resources/platform.png" width="700" >}}

This platform is used to validate the kinetic energy constraints developped during my PhD. An example of such experiment is shown in the video bellow. 

{{<vimeo 471886584>}}

<br> </br> 

The two following tables show the results of the measures over 10 trials for different velocities. 

$$\begin{array}{c:cc}  & Mean (J) & \text{std dev} (J)  \\\\\\ Robot & 0.1938 & \hline 0.0078 \\\\\\ Platform & 0.1993 & 0.0077 \\\\\\ Error & 0.0055 (2.84\\%)& \hdashline 0.0021  \end{array} $$

$$ \text{ Mean energy and standard deviation of the measure for a velocity of 30cm/s }$$    

$$\begin{array}{c:cc}  & Mean (J) & \text{std dev} (J)  \\\\\\ Robot & 0.5156 & \hline 0.0102 \\\\\\ Platform & 0.5285 & 0.0083 \\\\\\ Error & 0.0129 (3.17\\%)& \hdashline 0.0105  \end{array} $$

$$ \text{ Mean energy and standard deviation of the measure for a velocity of 50cm/s }$$ 

The repeatability of the platform is good and the measured errors are sufficiently small to validate that the kinetic energy measured through the model correctly computed.