---
title: "Torque regularisation"
date: 2017-01-02T23:53:00+01:00
draft: false
hideLastModified: true
summary: "Expressed a regularization that compensate the gravity induced torques."
summaryImage: "Resources/redondance.jpg"
tags: ["custom_image", "custom_summary"]
math: true
---

A redundant robot possesses more degrees of freedom than necessary to perform its task. This redundancy is handles inside a QP formulation using a regulariwation task. 

It can be interessting to keep the servoing of a point while performing motion in the null-space of the main task Jacobian by physically manipulating the robot. Such interaction can be useful in applications where the robot is sharing its workspace with a human (see Table \ref{table:Table_control_mode}). To enable such type of interaction, the following regularization task can be used: 

$$ R(\boldsymbol{\tau})  =  ||{\boldsymbol{\tau} - \boldsymbol{g(\boldsymbol{q})}||^2_{W\_0} }. $$

Where $ \boldsymbol{g(\boldsymbol{q})} \in \mathbb{R}^3$ is the gravity induce torque vector and $W\_0$ is a weighting matrix used to ensure that the regularization task doesn't disturbe the main task. This regularization function minimizes the difference between the computed torque and the gravity induced external torque. It results that the combination of joints that does not contribute to the resolution of the main tasks are compensating for gravity. 

This regularization is inserted inside the following QP problem :


$$ \begin{matrix} \boldsymbol{\tau}^{opt} = & \displaystyle \arg\min\_{\boldsymbol{\tau}} & || \dot{v}^* - \dot{v}(\tau) ||^2_2 + ||{\boldsymbol{\tau} - \boldsymbol{g(\boldsymbol{q})}||^2_{W\_0} } \\\\\\ & \textrm{s.t.} & A \boldsymbol{\tau} \le \boldsymbol{b} \end{matrix} $$

The following video uses such regularization. Most experiments where the robot is controller at the joint torque level use this regularization task.

{{< vimeo 695362226>}}

As it can be seen at the begining of the video, the robot end-effector is tracking a desired pose. The redundant degrees of freedom are used to compensate for gravity and can be used to explore the robot nullspace.